#!/usr/bin/env python3

# Copyright 2018 [@Coffee:matrix.org](https://matrix.to/#/@Coffee:matrix.org)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# The full text of the License should have been distributed with
# this file in a file called "LICENSE", and can also be obtained
# from https://spdx.org/licenses/Apache-2.0.html.


# stdlib
import argparse
import ast
import collections
import curses
import json


def read_data(filename, lineno, converter):
	with open(filename, "r", encoding="UTF-8") as f:
		if lineno is None:
			l = f.read()
		else:
			for dummy in range(lineno):
				l = f.readline()
		return converter(l)

def get_json(filename, lineno):
	return read_data(filename, lineno, json.loads)

def get_pson(filename, lineno):
	return read_data(filename, lineno, ast.literal_eval)

def rich_keys(obj, keys, maxlen=10):
	rich_keys = []
	#if isinstance(obj, dict):	keys = obj.keys()
	#elif isinstance(obj, list):	keys = range(len(obj))
	#else: raise TypeError("Unsupported type: " + repr(obj))

	for k in keys:
		o = obj[k]
		if isinstance(o, dict):
			if len(o) == 0:	rk = "{}:{{}}".format(k)
			else:		rk = "{}:{{{}}}".format(k, len(o))
		elif isinstance(o, list):
			if len(o) == 0:	rk = "{}:[]".format(k)
			else:		rk = "{}:[{}]".format(k, len(o))
		else:
			r = repr(o)
			if len(r) > maxlen: r = r[:maxlen - 3] + "..."
			rk = "{}:{}".format(k, r)

		rich_keys.append(rk)

	return rich_keys

def pretty_string(obj, maxlen, maxdepth=1):
	if isinstance(obj, dict):
		if maxdepth > 0:	r = ", ".join(obj.keys())
		elif len(obj) == 0:	r = "{}"
		else:			r = "{{{}}}".format(len(obj))
	elif isinstance(obj, list):
		if maxdepth > 0:	r = pretty_string(obj, maxlen, maxdepth - 1)
		elif len(obj) == 0:	r = "[]"
		else:			r = "[{}]".format(len(obj))
	else:
		r = repr(obj)

	if len(r) > maxlen: r = r[:maxlen - 5] + "..."

	if isinstance(obj, dict): return "{{{}}}".format(r)
	elif isinstance(obj, dict): return "[{}]".format(r)
	else: return r


class SliceDisplay:
	def __init__(self, window, slicecursor):
		self.window = window
		self.slicecursor = slicecursor
		self.rich_keys = rich_keys(slicecursor.slice, slicecursor.keys)

		self.maxlen = max(map(len, self.rich_keys)) + 1

		self.max_y, self.max_x = self.window.getmaxyx()
		self.slicecursor.per_line = self.max_x // self.maxlen

	def show(self):
		total_height = 3 + (len(self.rich_keys) - 1) // self.slicecursor.per_line
		x_active = 3 + self.slicecursor.active // self.slicecursor.per_line
		shift = 0

		if x_active >= self.max_y - 5:
			shift = max(0, min(x_active - self.max_y + 5, total_height - self.max_y + 1))

		#self.window.addstr(0, 40, "{} {} {} {}".format(x_active, total_height, shift, self.max_y))
		#self.window.clrtoeol()

		for i, e in enumerate(self.rich_keys):
			y = 3 + (i // self.slicecursor.per_line) - shift
			x = (i % self.slicecursor.per_line) * self.maxlen

			if y < 3 or y >= self.max_y: continue

			f = curses.A_REVERSE if i == self.slicecursor.active else 0
			#if x == 0: self.window.clrtoeol()
			self.window.addstr(y, x, str(e), f)
			self.window.clrtoeol()

		#self.window.clrtoeol()


class SliceCursor:
	def __init__(self, obj, per_line=1):
		self.slice = obj
		self.keys = None
		self.per_line = per_line

		if isinstance(obj, dict):
			self.keys = sorted(obj.keys())
		elif isinstance(obj, list):
			self.keys = range(len(obj))

		self.active = 0
		self.maxlen = max(map(lambda x: len(str(x)), self.keys)) + 1

	def get_key_at_cursor(self):
		return self.keys[self.active]

	def get_value_at_cursor(self):
		return self.slice[self.get_key_at_cursor()]

	def move_forward(self):
		self.active = (self.active + 1) % len(self.keys)

	def move_back(self):
		self.active = (self.active - 1) % len(self.keys)

	def move_up(self):
		self.active = (self.active - self.per_line) % len(self.keys)

	def move_down(self):
		self.active = (self.active + self.per_line) % len(self.keys)

	def move_by(self, amount):
		self.active = (self.active + amount) % len(self.keys)

	def __len__(self):
		return len(self.keys)

	def __iter__(self):
		return iter(self.keys)


class JSONCursor:
	def __init__(self, json):
		self.json = json
		self.keystack = collections.deque()
		self.slicestack = collections.deque()

		self.slicestack.append(SliceCursor(json))

	def enter(self, key=None):
		if key is None:
			key = self.get_slice().get_key_at_cursor()

		self.keystack.append(key)
		self.slicestack.append(SliceCursor(self.get_at_cursor()))

	def can_enter(self, key=None):
		if key is None:
			key = self.get_slice().get_key_at_cursor()

		obj = self.get_at_cursor()[key]
		return isinstance(obj, (dict, list)) and len(obj) > 0

	def backtrack(self):
		self.slicestack.pop()
		return self.keystack.pop()

	def get_slice(self):
		return self.slicestack[-1]

	def get_at_cursor(self):
		if len(self.keystack) == 0: return self.json
		obj = self.json
		for k in self.keystack:
			obj = obj[k]
		return obj

	def key_string(self):
		return " -> ".join(map(repr, self.keystack))


def main(stdscr, jsondata, options):
	curses.halfdelay(255)
	my, mx = stdscr.getmaxyx()

	j = jsondata
	c = JSONCursor(j)
	s = c.get_slice()
	d = SliceDisplay(stdscr, s)

	d.show()
	stdscr.refresh()

	while True:
		k = stdscr.getch()
		if k == -1: continue
		if k in (ord('q'), ord('x'), curses.KEY_BREAK, curses.KEY_CLOSE, curses.KEY_EXIT): break
		#stdscr.addstr(str(c))

		if k == curses.KEY_LEFT:
			s.move_back()
		elif k == curses.KEY_RIGHT:
			s.move_forward()
		elif k == curses.KEY_UP:
			s.move_up()
		elif k == curses.KEY_DOWN:
			s.move_down()
		elif k == curses.KEY_PPAGE:
			s.move_by(-my * s.per_line // 2)
		elif k == curses.KEY_NPAGE:
			s.move_by(my * s.per_line // 2)
		elif k in (8, 27, ord('b'), curses.KEY_BACKSPACE, curses.KEY_CANCEL, curses.KEY_UNDO):
			stdscr.erase()
			c.backtrack()
			s = c.get_slice()
			d = SliceDisplay(stdscr, s)
			stdscr.addstr(0, 0, c.key_string())
			stdscr.clrtoeol()
		elif k in (10, 13, ord(' '), curses.KEY_ENTER, curses.KEY_COMMAND, curses.KEY_OPEN):
			if c.can_enter():
				stdscr.erase()
				c.enter()
				s = c.get_slice()
				d = SliceDisplay(stdscr, s)
				stdscr.addstr(0, 0, c.key_string())
				stdscr.clrtoeol()
		else:
			continue

		d.show()
		stdscr.addstr(1, 0, pretty_string(s.get_value_at_cursor(), mx))
		stdscr.clrtoeol()
		stdscr.refresh()

def greaterthanoneint(string):
	value = int(string)
	if value < 1:
		msg = "{!r} is not a valid line number. Must be 1 or greater.".format(string)
		raise argparse.ArgumentTypeError(msg)
	return value

def parse_args():
	parser = argparse.ArgumentParser(description="Simple graphical console-based JSON viewer.")
	parser.add_argument("jsonfile", help="File to display.")
	parser.add_argument("-l", "--line", nargs='?', const=1, type=greaterthanoneint, help="Read this line number and treat it as a whole json stanza. Default: read the whole file.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debugging.")
	parser.add_argument("-p", "--python", action='store_true', default=False, help="Interpret the file as a (nested) python object instead.")
	options = parser.parse_args()
	return options

if __name__ == '__main__':
	options = parse_args()
	if options.python:	jsondata = get_pson(options.jsonfile, options.line)
	else:			jsondata = get_json(options.jsonfile, options.line)
	curses.wrapper(main, jsondata, options)

