# JSONBrowser
[JSONBrowser](https://gitlab.com/Matrixcoffee/JSONBrowser) is a simple graphical console-based JSON browser/viewer.

## Why does it exist?
I find myself working with [JSON](https://en.wikipedia.org/wiki/JSON) files a lot lately. I wrote this to be able to quickly inspect specific deeply-nested items in large trees, and to help me understand the structure of given JSON files.

It also has some special features such as being able to read "one-json-tree-per-line" files, and Python object trees. (Basically what you get if you do `print(repr(jsontree))` instead of using a JSON writer. I'll dub this format "PSON".)

## Status
**Alpha**. It works for my purposes, but it's pretty bare and raw.

## Recommended Installation
```
# Prerequisites
$ apt-get install python3 git

# Main component
$ git clone https://gitlab.com/Matrixcoffee/JSONBrowser.git
```
Simply copy the file `JSONBrowser.py` to any directory in your `PATH`, and make sure the executable flag is set.

## How to Run
Run it directly out of the git repo:
```
$ python3 JSONBrowser.py --help
```
Or if copied to a directory in your `PATH`:
```
$ JSONBrowser.py --help
```
Happy viewing!

## Contributing
Anyone can contribute to the project. Please send [MR](https://gitlab.com/help/user/project/merge_requests/index.md)s, or open an issue on the [tracker](https://gitlab.com/Matrixcoffee/JSONBrowser/issues). JSONBrowser has a [Matrix](https://matrix.org) room at [#jsonbrowser:matrix.org](https://matrix.to/#/#jsonbrowser:matrix.org).

## License
[Apache-2.0](https://spdx.org/licenses/Apache-2.0.html)

Copyright 2018 [@Coffee:matrix.org](https://matrix.to/#/@Coffee:matrix.org)

   > Licensed under the Apache License, Version 2.0 (the "License");
   > you may not use this file except in compliance with the License.

   > The full text of the License can be obtained from the file called [LICENSE](LICENSE).

   > Unless required by applicable law or agreed to in writing, software
   > distributed under the License is distributed on an "AS IS" BASIS,
   > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   > See the License for the specific language governing permissions and
   > limitations under the License.

